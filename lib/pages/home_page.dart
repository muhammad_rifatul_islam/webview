import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_plus/webview_flutter_plus.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  late WebViewPlusController controller;

double progress=0;




  @override
  Widget build(BuildContext context) {
    return  WillPopScope(
       onWillPop: () async{
         if(await  controller.webViewController.canGoBack()){
           controller.webViewController.goBack();
           return false;
         }
         else{
           return true;
         }

       },
      child: Scaffold(
       appBar: AppBar(
         actions: [
           IconButton(onPressed: () async {


              if(await  controller.webViewController.canGoBack()){
                controller.webViewController.goBack();
              }

           }, icon: Icon(Icons.arrow_back))
         ],
       ),
        body: SafeArea(
          child: Column(

            children: [
              LinearProgressIndicator(
                value: progress,
                backgroundColor: Colors.yellow,

              ),

              Expanded(
                child: WebViewPlus(
                //  initialUrl: 'https://www.prothomalo.com/',
                  initialUrl: 'assets/index.html',
                  javascriptMode: JavascriptMode.unrestricted,

                  onWebViewCreated: (controller){
                     this.controller=controller;

                    // controller.loadUrl('assets/index.html');
                  //  loadLocalHtml();
                  },


                  onProgress: (int progress) {
                    setState(()=> this.progress=progress/100);
                    print("WebView is loading (progress : $progress%)");
                  },

                ),
              ),
            ],
          ),

        ),floatingActionButton: FloatingActionButton(

          onPressed: () async{
            controller.loadUrl('https://www.amazon.com/');
          },
        child: Icon(Icons.import_export_outlined,size: 40,),

      ),
      ),
    );
  }


 /*void loadLocalHtml() async{
    
    final html= await rootBundle.loadString('assets/index.html');
    final url=  Uri.dataFromString(
      html,
          mimeType:'text/html',
      encoding: Encoding.getByName('utf-8'),
      ).toString();
     controller.loadUrl(url);
  }*/
}
